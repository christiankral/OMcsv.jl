using Base, DelimitedFiles, Printf
using DelimitedFiles, ArgParse
using OMJulia

# model = "TestOMcsv.OhmsLaw"
# file = "TestOMcsv/package.mo"
# csvFile = "TestOMcsv/Resources/OhmsLaw/OhmsLaw.csv"
# csvresFile = "TestOMcsv/Resources/OhmsLaw/OhmsLaw_results.csv"
# classesArg = ""

model = "TestOMcsv.Exp"
file = "TestOMcsv/package.mo"
csvFile = "TestOMcsv/Resources/Exp/Exp.csv"
csvresFile = "TestOMcsv/Resources/Exp/Exp_results.csv"
classesArg = "TestOMcsv.Exp:TestOMcsv.Data"

sysLibsArg = "Modelica@4.0.0"
sysLibs = split(sysLibsArg, ":")
classes = split(classesArg, ":")
margin = 0
header = 2
delimiter = '\t'
build = true
verbose = true

include("../src/loadModel.jl")
include("../src/csv.jl")

#!/bin/bash
export LOCALDIR=$PWD
export WORK=/work
cp -fR ../resources/TestOMcsv $WORK
cd $WORK

OMcsv.sh TestOMcsv/package.mo TestOMcsv.OhmsLaw \
TestOMcsv/Resources/csv/OhmsLaw.csv \
TestOMcsv/Resources/csv/OhmsLaw_results.csv \
--delimiter TAB --build --verbose

OMcsv.sh TestOMcsv/package.mo TestOMcsv.Exp \
TestOMcsv/Resources/csv/Exp.csv \
TestOMcsv/Resources/csv/Exp_results.csv \
--class TestOMcsv.Data \
--delimiter TAB --build --verbose --create Exp

cd $LOCALDIR

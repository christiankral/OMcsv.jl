# Read header, margin and data from CSV file
using DelimitedFiles
function csvRead(csvFile; header = 2, margin = 0, delimiter = '\t')
    if !isfile(csvFile)
        error("    File "*csvFile*" does not exist")
    end
    csvContent = readdlm(csvFile, delimiter, skipstart = 0, quotes = false)
    csvHeader = csvContent[1:header,:]
    csvMargin = csvContent[:,1:margin]
    csvData = csvClean(csvContent[header+1:end, margin+1:end])
    return csvHeader, csvMargin, csvData
end

# Remove non-numeric data from numeric data section
function csvClean(csvData)
    # Change data values to 0, if string is loaded
    for r = 1:size(csvData, 1)
        for c = 1:size(csvData, 2)
            if !isa(csvData[r,c], Number)
                csvData[r,c] = NaN
            end
        end
    end
    return csvData
end

# Determine row of longest entry
function validDataRow(csvIndividualData)
    nColumn = zeros(size(csvIndividualData, 1))
    for r in 1:size(csvIndividualData, 1)
        for c in 1:size(csvIndividualData, 2)
            # Check all columns c of rows r if at least one of them is numeric
            if typeof(csvIndividualData[r,c]) == Float64 ||
                typeof(csvIndividualData[r,c]) == Int64
              # If there exists at least one columns with float entry in row r,
              # then store row r in rIndividual
              if !isnan(csvIndividualData[r,c])
                  # Consider only non-zero values
                  nColumn[r] = nColumn[r]+1
              end
            end
        end
    end
    # Determine row with maximum number of elements and the
    # maximum number of elements, maxRow
    maxRow, row = findmax(nColumn)
    # Determine number of rows which are equal to maxRow elements
    # If threre is more than one row, set valid=false, otherwise valid=true
    valid = sum(nColumn .== maxRow) == 1
    return row, valid
end

# Write header, margin and data from CSV file
function csvWrite(csvFile, csvHeader, csvMargin, csvData; header = 2, margin = 2, delimiter = delimiter)
    rDataEnd = size(csvData, 1)
    csvContent = cat(csvHeader, cat(csvMargin[header+1:rDataEnd+header,:], csvData, dims = 2), dims = 1)
    status = writedlm(csvFile, csvContent, delimiter, quotes = false)
    return status
end

#!/bin/bash
#=
exec julia --color=yes --startup-file=no "${BASH_SOURCE[0]}" "$@"
=#

__precompile__(true)

# Refs: https://docs.julialang.org/en/v1/manual/faq/#man-scripting
# This Julia script is open source under the BSD 3-clause license
# https://gitlab.com/christiankral/OMcsv.jl
# Check README.md and LICENSE
#
# 0.10.6 2024-09-05
#       Fix issue with unavailable variable `i`
# 0.10.5 2024-08-25
#       Clarify name of missing parameter
# 0.10.4 2022-06-13
#       Fix wrong OMC handler to be closed
# 0.10.3 2022-06-10
#       Close OMC session based on
#           https://github.com/OpenModelica/OMJulia.jl/issues/62
# 0.10.2 2022-06-10
#       Slight change of structure and fix of wrong global variable name
# 0.10.1 2022-06-06
#       Fix typo in `sigdigits`
# 0.10.0 2022-06-05
#       Add switch --sigdigits to to limit number of significatnt digits of
#           created result numbers
#       Change wording of modified (Mod) and result (Res) variables
# 0.9.0 2021-08-04
#       Add switch --create to create a Modelica package with all
#           parameter cases
#       Add switch --nosim to not simulate the model, just testing parameter
#           settings and optionally create Modelica package
# 0.8.1 2021-08-01
#       Improve handling of non-existing variables
#       Improve messages during parameter setting
# 0.8.0 2021-08-01
#       Add alternative class specification to handle structural parameteters
# 0.7.1 2021-07-20
#       Add bash script to start Julia script
#       Remove version from ArgParse
# 0.7.0 2021-07-20
#       Renamed OMScript.jl to OMcsv.jl
#       Change command line argument csvIn => csv and csvOut => csvResult
#       Change argument model to four mandatory arguments in following order
#       - file
#       - name
#       - csv
#       - csvResult
#       Add optional command line argument to build each model before simulating
# 0.6.0 2021-07-19
#       Restructure script to work without text files
# 0.5.0 2021-04-21
#       Switch to ArgParse model
#       Remove option of using environmental variable file (env)

using Base
using Printf
using Dates
using Format
using DelimitedFiles
using ArgParse
using OMJulia

println("OMcsv.jl 0.10.6 (C) Christian Kral 2024-09-05")

function parse_commandline()
    s = ArgParseSettings(description="""
  Perform OpenModelica simulations based on CSV parameter files
  """,
        commands_are_required=true)

    @add_arg_table s begin
        "file"
        help = "Modelica file to be loaded, containing MODEL"
        required = true
        "model"
        help = "Modelica model to be simulated (incl. dot notation); make sure to apply annotation(uses(...)) to specify explicit dependenvies on other libraries; these libraries are loaded automatically when stored in your local work directory or available as system librararies or maintained by the OM PackageManager"
        required = true
        "csv"
        help = "CSV parameter file"
        required = true
        "csvResult"
        help = "CSV result output file"
        required = true
        "--sysLibs"
        help = "Modelica system libraries to be used, separated by :\nExample Modelica@3.2.3\nIf the uses annotation specifies all required system libraries, this option is not required"
        default = ""
        "--classes"
        help = "Modelica classes containing structural parameters which are extended in MODEL; multiple classes may be sperated by : (colon); this specification is solely required for sturctural parameters which change the number of states"
        default = ""
        "--margin"
        help = "Skip MARGIN columns when reading CSV data from file (left margin)"
        arg_type = Int
        default = 0
        "--header"
        help = "The header consists of HEADER rows; the first row has to contain the parameter names, all subsequent rows are skipped (and may contain units or other meta data ignored) when reading the CSV data file; the minmum value of HEADER is 1"
        arg_type = Int
        default = 2
        "--delimiter"
        help = "Delimiter of CSV files; typical options are SPACE , ; : TAB where TAB = tabular character"
        default = "TAB"
        "--build"
        help = "Build (compile) each model before running the simulation; this option is required if structural parameters are used and the number of states changes from one parameter set to another"
        action = :store_true
        "--nosim"
        help = "Do not perform simulation, but create OpenModelica model instance and set parameters; if BUILD is set, yet building of the model will ot be performed "
        action = :store_true
        "--create"
        help = "Create Modelica package CREATE.mo (extension .mo is automatically added) containing models for each parameter case; a package path may be specified; make sure the package name is Modelica compliant as this is not checked; the file CREATE.mo will be overwritten without asking, if the file already exists"
        default = ""
        "--sigdigits"
        help = "Number of significant digits of results stored in result CSV file"
        arg_type = Int
        default = 6
        "--verbose"
        help = "Log detailed work in progress, if used"
        action = :store_true
    end

    return parse_args(s)
end

# Created formated string from number of case (r) and maximum number
# of cases (rmax) with equal number of digits for all cases
function formatcase(r; rmax=100)
    digmax = Int(round(log10(rmax), RoundUp))
    digstr = "%0" * string(digmax) * "i"
    return cfmt(digstr, r)
end

# Create Modical compliant name out of model and case number r
function localmodel(model, r; rmax=100)
    return split(model, ".")[end] * "_" * formatcase(r, rmax=rmax)
end

# Parse arguments
args = parse_commandline()

# Determine model name to be simulated
model = get(args, "model", nothing)
# Determine files to be loaded
file = get(args, "file", "")
# Determine Modelica system libraries to be used
sysLibsArg = get(args, "sysLibs", "")
sysLibs = split(sysLibsArg, ":")
# Multiple classes extended in MODEL, where structural parameters are stored
classesArg = get(args, "classes", "")
classes = split(classesArg, ":")
# Number of first columns skipped when reading parameter CSV file (left margin)
margin = get(args, "margin", 0)
# Number of header rows ignored when reading parameter values; first row
# contains parameter names
header = get(args, "header", 2)
# Switch for creating Modelica package CREATE as CREATE.mo
create = get(args, "create", "")

# Determine CSV parameter file
csvFile = get(args, "csv", nothing)
# Determine CSV result file
csvresFile = get(args, "csvResult", nothing)
# Determine delilimiter
delimiter = get(args, "delimiter", "TAB")
if delimiter == "TAB"
    # Change 'TAB' to '\t'
    delimiter = '\t'
elseif delimiter == "SPACE"
    # Change 'SPACE' to ' '
    delimiter = ' '
end
# Otherwise just use the proagated character

# Optionally build each model before simulating
build = get(args, "build", false)
# Do not perform simulation
nosim = get(args, "nosim", false)
# Number of significant digits of results stored in CSV files
sigdigits = get(args, "sigdigits", 6)
# Optional verbose mode
verbose = get(args, "verbose", false)

# Include default CSV functions and loadModel
include("csv.jl")
include("loadModel.jl")

if verbose
    # Report variables
    println("Mandatory arguments:")
    println("    Modelica file             = ", file)
    println("    Simulation model          = ", model)
    println("    CSV parameter file        = ", csvFile)
    println("    CSV result file           = ", csvresFile)
    println("Optional arguments:")
    println("    header                    = ", header)
    println("    margin                    = ", margin)
    println("    Modelica system libraries = ", sysLibsArg)
    println("    build                     = ", build)
    println("    verbose                   = ", verbose)
end
# Read parameter CSV file
if verbose
    println("Reading files:" * csvFile)
end
csvHeader, csvMargin, csvData = csvRead(csvFile, header=header, margin=margin, delimiter=delimiter)

# Read result CSV file
if verbose
    println("Reading files:" * csvresFile)
end
csvResultHeader, csvResultMargin, csvResultData =
    csvRead(csvresFile, header=header, margin=margin, delimiter=delimiter)

# Determine number of rows and columns of CSV input file
rows = size(csvData, 1)
cols = size(csvData, 2)
# Determine number of rows and columns of CSV result file
rowsResult = size(csvResultData, 1)
colsResult = size(csvResultData, 2)
if rows != rowsResult
    # Trigger error if number of rows differ between parameter and result CSV
    # file
    error("\nNumber of rows of CSV file (", csvFile, ": ", rows, ") and\nCSV result file (", csvresFile, ": ", rowsResult, ") are not equal")
end

# Log model and files
if verbose
    println("    Load Modelica model " * model)
    println("              from file " * file)
end

# Create CREATE.mo if switch is used
if create != ""
    # Create Modelica file name and change extension to .mo if required
    moFile = splitext(create)[1] * ".mo"
    # Extract file package name from CREATE
    package = splitext(splitpath(create)[end])[1]
    # Open file for writing
    mo = open(moFile, "w")
    write(mo, "package " * package * " \"Created by OMcsv at " * string(now()) * "\"\n")
    write(mo, "  // CSV parameter file: " * csvFile)
    if verbose
        println("Create Modelica package " * moFile * " containing all simulation cases")
    end
end

# Modelica system libraries must be specified by: annotation(uses(...))
# an will be loaded automatically then, see
# https://github.com/OpenModelica/OMJulia.jl/issues/44#issuecomment-873365846
# LOADING MODEELICA SYSTEM LIBRARIES DOES NOT WORK WITH SPECIFIED VERSION NUMBER
global mod
mod = OMJulia.OMCSession()
status = ModelicaSystem(mod, file, model)
if ~isnothing(status)
    error(status)
end

# Go through all rows
global csvResultData
for r in 1:rows
    # Log case
    println("Processing Modelica case #" * formatcase(r, rmax=rows))
    if create != ""
        # Write extended model with parameters to file
        if create != ""
            write(mo, "\n")
            write(mo, "  model " * localmodel(model, r, rmax=rows) * "\n")
            write(mo, "    extends " * model * "(\n")
        end
    end

    # Cycle through all simulation cases
    for c in 1:cols
        # Get parameter name
        par = csvHeader[1, c]
        # Get parameter value (as number)
        val = csvData[r, c]
        # Convert parameter value to string
        valstr = string(val)
        if verbose
            print("    Mod " * par * " = " * valstr * ": ")
        end

        # Change method of setting parameters from
        #     par = csvHeader[1,c] * " = " * string(csvData[r,c])
        #     setParameters(mod, par) to sendExpression(...)
        # Refs: https://github.com/OpenModelica/OMJulia.jl/issues/45
        # Refs: https://github.com/OpenModelica/OMJulia.jl/issues/47
        # Refs: https://github.com/OpenModelica/OMJulia.jl/pull/48
        global changed = false
        global status
        # Get status of parameter par
        status = OMJulia.getQuantitiesHelper(mod, par, verbose=false)
        if length(status) == 0
            println("Parameter \'", par, "\' does not exist in model => ignoring")
        else
            # Write parameter to file CREATE
            # Independent of a possibly failing attempt to set the parameter;
            # even if CLASS has not fully specified the correct Modelica
            # class containing a structural parameter
            if create != ""
                if c != 1
                    # Write semcolon after parameter except for last line
                    write(mo, ",\n")
                end
                write(mo, "      " * par * " = " * valstr)
            end

            # Test if parameter is non-structural (and can be changed)
            status = OMJulia.isParameterChangeable(mod, par, val, verbose=false)
            if status
                # Regular (non-structural) parameters are change directly
                setParameters(mod, par * "=" * valstr)
                if verbose
                    println("OK")
                end
            else
                # Structural parameter changes
                try
                    # Try first to change structural parameter in MODEL
                    status = sendExpression(mod, "setParameterValue(" * model * "," * par * "," * valstr * ")", parsed=false)
                    if status == "Ok\n"
                        changed = true
                        if verbose
                            println("in " * model)
                        end
                    end
                catch
                end
                if !changed
                    # If not yet successful, try to change structural parameter
                    # in the models specfied in CLASSES
                    for i in collect(1:length(classes))
                        if length(classes[i]) != 0
                            try
                                status = sendExpression(mod, "setParameterValue(" * classes[i] * "," * par * "," * valstr * ")", parsed=false)
                                if status == "Ok\n"
                                    changed = true
                                    if verbose
                                        println("in " * classes[i])
                                    end
                                end
                            catch
                            end
                        end
                        # If parameter could not be changed CLASSES may not be
                        # containing the location of where the parameter is defined
                        if !changed
                            println(" COULD NOT BE CHANGED:")
                            println("        ",classes[i])
                            println("        ",par, "=", valstr)
                            println("        The simulation result might not be correct")
                        end
                    end
                end
            end
        end

    end
    # Write end of model
    if create != ""
        write(mo, ");\n")
        write(mo, "  end " * localmodel(model, r, rmax=rows) * ";\n")
    end

    # If NOSIM is not set, the model may be built and simulated
    if !nosim
        try
            if build
                OMJulia.buildModel(mod)
            end
            simulate(mod)
        catch
            error("\nBuilding or running the simulation failed")
        end

        # Read variable names stored in model
        vars = getSolutions(mod)
        # Read results at end of simulation time
        for c in collect(1:colsResult)
            # Unknown phenomenon: function string has to be used as the
            # the determination of val otherwise fails
            var = string(csvResultHeader[1, c])
            # Check if var is a valide variable (listed in vars)
            # Only in case of var being a valid variable it is stored in
            # CSV result array
            if !isempty(vars[vars.==var])
                val = getSolutions(mod, var)[end][end]
                csvResultData[r, c] = val
                if verbose
                    println("    Res " * var * " = " * string(val))
                end
            else
                if verbose
                    # Log that variable is available
                    println("    Not " * var)
                end
            end
        end
    else
        # No simulation is performed
        if verbose
            println("    Model is not simulated due to --nosim")
        end
    end

end

# Finish created Modelica file and close it
if create != ""
    write(mo, "end " * package * ";\n")
    if verbose
        println("Writing Modelica file")
    end
    close(mo)
end

# Write result CSV file
println("Writing result CSV file")
csvWrite(csvresFile, csvResultHeader, csvResultMargin,
    round.(csvResultData, sigdigits=sigdigits),
    header=header, margin=margin, delimiter=delimiter)
println("Done\n")

# Close OMC session
sendExpression(mod, "quit()", parsed=false)

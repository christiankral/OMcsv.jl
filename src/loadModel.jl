using OMJulia, Base

function loadModel(omc, model; verbose = false)
    if model == ""
        if verbose
            println("    No Modalica system libraries are loaded explicitly")
        end
    else
        # Optinally allow verison number, e.g. model = "Modelica@3.2.3"
        model_version = split(model, "@")
        len = size(model_version, 1)
        name = model_version[1]
        if len == 1
            # Not @ separataor, thus there is no version number provided
            if verbose
                print("    loadModel("*name*")")
            end
            status = OMJulia.sendExpression(omc, "loadModel("*name*")")
        elseif len == 2
            version = model_version[2]
            # Only one @ separator
            if verbose
                print("    loadModel("*name*",{\""*version*"\"})")
            end
            status = OMJulia.sendExpression(omc, "loadModel("*name*",{\""*version*"\"})")
        else
            # More than one @ separators
            error("loadModel: More than one spearator @ found in model string: " * model)
        end
        if verbose
            if status==true
                # Report successful operation
                println(" ... successful")
            else
                # Indicate failed operation and print status message
                println(" ... failed")
                println("    Status: " * status)
            end
        end
    end
end

# OMcsv.jl

![OMcsv logo](/resources/logo/OMcsv.png)

- Perform OpenModelica simulations based on CSV parameter files
- For the usage with OpenModelica OMcsv partially replaces the Modelica package [KeyWordIO](https://github.com/christiankral/KeyWordIO)

## 1 Purpose

The bash script `OMcsv.sh` calls the Julia script `src/OMcsv.jl` and fulfills the following tasks:

- Parameterize and simulate a Modelica model several times
- Use the parameters stored in a CSV file
- Write simulation results in a result CSV file
- The Modelica model need no special treatment; it only needs to translate and simulate in OpenModelica with no error
- Modelica examples are provided in `resources/TestOMcsv/package.mo`

### 1.1 Example Ohm's Law

![OhmsLaw](/resources/TestOMcsv/Resources/Images/OhmsLaw.png)

- Check out model `TestOMcsv.OhmsLaw` in OpenModelica
- An electrical analog circuit consists of a resistor and optionally, if the structural integer parameter `useInductor <> 1`, a series connected inductor
- In case of `useInductor == 1`, the inductor is conditionally replaced by a short; in this case the load solely consists of a pure resistor and the inductor is not used; this is a steady state case
- Parameters of the model are
    - Voltage `V`
    - Resistance `R`
    - Inductance `L`
    - Stuctural integer parameter `dyn`
- The result of the model are two variables at the end of the simulation time = `2s`
    - Current `I = resistor.i`
    - Power `P = resistor.LossPower`
- If `useInductor <> 1` the
    - current `I` is equal to `V / R`
    - power `P` is equal to `V^2 / R`
- In case of `useInductor == 1` the inductor causes a steady rise of the current according to an exponential function; in this case the current `I` and the power `P` are smaller than in the steady state case `useInductor <> 1`
- The parameter CSV file `TestOMcsv/Resources/csv/OhmsLaw.csv` consists:

![](resources/TestOMcsv/Resources/Images/OhmsLaw.csv.png)

- In this parameter CSV file
    - the header consists of two rows
    - the first row contains the parameter names
    - the second row contains unit if parameters (not processed)
    - the delimiter is tabular `\t`
- In case parameter name used in the parameter CSV cannot be found in the Modelca model, this parameter is ignored
- The results are written to the result CSV file `TestOMcsv/Resources/csv/OhmsLaw_result.csv`
- Originally the result CSV file only contains zeros and has to have the same number of rows; empty cells are not allowed
    - The first row contains the variable names which are stored in the file (at the stop time of the simualtion)
    - The second row contains the units of the variables (not preocessed)

![](resources/TestOMcsv/Resources/Images/OhmsLaw_result.csv.png)

- After running `OMcsv.sh` through `test.sh` the result CSV file is overwritten by the simulation results

![](resources/TestOMcsv/Resources/Images/OhmsLaw_result_written.csv.png)

- In case the result CSV file contains some rows with variable names which are not used in the Modelica model, these results will be ignored and not changed in ther CSV parameter file

See subsection 4.1 on how to run this example

### 1.2 Exponential Functions

The second example is a simplification of the example shown in 1.1 with some additional properties:

- Check out model `TestOMcsv.Exp` in OpenModelica
- The exponential function `der(x) = -d * x` is evaluated if the structural parameter `dyn == 1` (one state variable)
- In case of `dyn <> 1`, only the algebraic equation `x = c` is evaluated
- Parameters of the model are
    - `c` Starting value of the initial value of variable `x`
    - `d` Decay factor of the exponential factor (if `dyn == 1`)
    - `dyn` Stuctural integer parameter
- The parameters `c` and `dyn` are defined in `TestOMcsv.Data`, not `TestOMcsv.Exp`
- In the parameter file `Exp.csv` one additional parameter `t` is defined which is not used in the model; it will be ignored

![](resources/TestOMcsv/Resources/Images/Exp.csv.png)

- The result of the model is the variable `x` at the end of the simulation time = `1s`
- The result CSV file also contains the variable `y` which is not used in the model; it will be ignored
- The initial result CSV contains zeros where the results shall be written to; empty cells are not allowed

![](resources/TestOMcsv/Resources/Images/Exp_result.csv.png)

After running the OMcsv only column `x` is filled with results, as variable `y` is not used in the model `TestOMcsv.Exp`

![](resources/TestOMcsv/Resources/Images/Exp_result_written.csv.png)

See subsection 4.2 on how to run this example

## 2 Prerequisites and Download

- Julia installed
- Installed julia packages
    - OMJulia
    - DelimitedFiles
    - ArgParse
- Clone repository [OMcsv.jl](https://gitlab.com/christiankral/OMcsv.jl) to a user directory, e.g., `$HOME` by `git clone https://gitlab.com/christiankral/OMcsv.jl`

## 3 Customize script `OMcsv.sh`

- Copy the bash script `OMcsv.sh` to a directory which is accessible through the `PATH` environment variable, e.g., `/usr/local/bin/`
- Edit the bash script by `sudo nano /usr/local/bin/OMcsv.sh` and change the environment variable `OMCSVDIR` to the location of your cloned repository [OMcsv.jl](https://gitlab.com/christiankral/OMcsv.jl), e.g., `$HOME/OMcsv.jl/`
- Test if script works; in your home directory `OMcsv.sh --help` shall show the help screen

## 4 Steps to test this library

- Open a terminal and change to directory `test/`
- Make the bash script `test.sh` executable by using `chmod +x test.sh`
- Create work directory `/work` or change work directory `$WORK` in bash script `test.sh`
- Run `./test.sh`

### 4.1 Example Ohm's Law

```bash
OMcsv.sh TestOMcsv/package.mo TestOMcsv.OhmsLaw \
TestOMcsv/Resources/csv/OhmsLaw.csv \
TestOMcsv/Resources/csv/OhmsLaw_results.csv \
--delimiter TAB --build --verbose
```

Explantation of non-mandatory command line parameters:

- `--delimiter TAB` applies CSV file delimiter tab (`\t`)
- `--build` builds each model before simulating, as the structural variable `useInductor` is used
- `--verbose` is used to log each step of OMcsv

### 4.2 Example Exponential Function

```bash
OMcsv.sh TestOMcsv/package.mo TestOMcsv.Exp \
TestOMcsv/Resources/csv/Exp.csv \
TestOMcsv/Resources/csv/Exp_results.csv \
--class TestOMcsv.Data \
--delimiter TAB --build --verbose --create Exp
```
Explantation of non-mandatory command line parameters:

- `--delimiter TAB` applies CSV file delimiter tab (`\t`)
- `--build` builds each model before simulating, as the structural variable `useInductor` is used
- `create Exp` creates the file `Exp.mo` containing all parameter cases
- `--verbose` is used to log each step of OMcsv

Contents of file `Exp.mo`

```modelica
package Exp "Created by OMcsv at 2021-08-05T16:17:30.057"
  // CSV parameter file: TestOMcsv/Resources/csv/Exp.csv
  model Exp_001
    extends TestOMcsv.Exp(
      c = 1,
      d = 1,
      dyn = 0);
  end Exp_001;

  model Exp_002
    extends TestOMcsv.Exp(
      c = 2,
      d = 1,
      dyn = 1);
  end Exp_002;

  model Exp_003
    extends TestOMcsv.Exp(
      c = 2,
      d = 1,
      dyn = 0);
  end Exp_003;

  model Exp_004
    extends TestOMcsv.Exp(
      c = 2,
      d = 5,
      dyn = 1);
  end Exp_004;

  model Exp_005
    extends TestOMcsv.Exp(
      c = 2,
      d = 5,
      dyn = 0);
  end Exp_005;
end Exp;
```

# 5 Contact

In case you have any questions, feature requests or bugs to report, please [open an issue at GitLab](https://gitlab.com/christiankral/OMcsv.jl/-/issues/new)

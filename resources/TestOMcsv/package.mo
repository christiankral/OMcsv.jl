within ;
package TestOMcsv "Package to test Julia script OMcsv.jl"
  extends Modelica.Icons.ExamplesPackage;
  import Modelica.Units.SI;

  annotation (uses(Modelica(version="4.0.0")),
    version="1",
    conversion(noneFromVersion=""));
end TestOMcsv;

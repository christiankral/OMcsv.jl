within TestOMcsv;
model OhmsLaw
  extends Modelica.Icons.Example;
  parameter SI.Voltage V = 100 "Supply voltage";
  parameter SI.Resistance R = 100 "Load resistance";
  parameter SI.Inductance L = 100 "Load inductor";
  parameter Integer useInductor = 1 "Use inductor if true";
  SI.Current I = resistor.i "Current";
  SI.Power P = resistor.LossPower "Power";
  Modelica.Electrical.Analog.Basic.Resistor resistor(R = R) annotation (
    Placement(visible = true, transformation(extent = {{-20, 20}, {0, 40}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (
    Placement(transformation(extent = {{-40, -60}, {-20, -40}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = V) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin = {-30, 0})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (
    Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, 10}, {10, -10}}, rotation = 270)));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L = L)  if useInductor == 1 annotation (
    Placement(visible = true, transformation(origin = {30, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.Short short if useInductor <> 1 annotation (
    Placement(visible = true, transformation(origin = {30, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(constantVoltage.p, resistor.p) annotation (
    Line(points = {{-30, 10}, {-30, 30}, {-20, 30}}, color = {0, 0, 255}));
  connect(ground.p, constantVoltage.n) annotation (
    Line(points = {{-30, -40}, {-30, -10}}, color = {0, 0, 255}));
  connect(ground.p, currentSensor.n) annotation (
    Line(points = {{-30, -40}, {-30, -30}, {70, -30}, {70, -10}}, color = {0, 0, 255}));
  connect(inductor.p, resistor.n) annotation (
    Line(points = {{20, 50}, {10, 50}, {10, 30}, {0, 30}}, color = {0, 0, 255}));
  connect(currentSensor.p, inductor.n) annotation (
    Line(points = {{70, 10}, {70, 30}, {50, 30}, {50, 50}, {40, 50}}, color = {0, 0, 255}));
  connect(resistor.n, short.p) annotation(
    Line(points = {{0, 30}, {10, 30}, {10, 10}, {20, 10}}, color = {0, 0, 255}));
  connect(short.n, currentSensor.p) annotation(
    Line(points = {{40, 10}, {50, 10}, {50, 30}, {70, 30}, {70, 10}}, color = {0, 0, 255}));
  annotation (
    experiment(StartTime = 0, StopTime = 2, Tolerance = 1e-06, Interval = 0.01));
end OhmsLaw;
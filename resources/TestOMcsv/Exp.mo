within TestOMcsv;

model Exp
    extends TestOMcsv.Data;
    extends Modelica.Icons.Example;
    parameter Real d = 3;
    Real x(start = c);
equation
    if dyn == 1 then
        der(x) = -d * x;
    else
        x = c;
    end if;
annotation(
    experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-6, Interval = 0.002));
end Exp;